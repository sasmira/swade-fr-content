# Savage Worlds ADventure Edtion par [Pinnacle Entertainment Group](https://www.peginc.com/)

<div align="center">![peg_logo_swade](./peg_logo_swade.png)</div>

Module pour système de jeu SWADE pour [Foundry Virtual Tabletop](https://foundryvtt.com/) qui fournit une traduction de la fiche de personnage et des systèmes de base pour jouer à Savage Worlds ADventure Edition de [Pinnacle Entertainment Group](https://www.peginc.com/).
- Vous devez posséder la version payante du module [Savage Worlds Adventure Edition Core Rules for Foundry VTT](https://www.peginc.com/store/savage-worlds-adventure-edition-core-rules-pdf-swade/), 
- Le livre de règles en français, disponible chez [Black Book Édition](https://www.black-book-editions.fr/produit.php?id=9012), est nécessaire afin de pouvoir exploiter pleinement le module en français.

## Installer avec le manifeste
Copier ce lien et chargez-le dans le menu module de Foundry.
> https://gitlab.com/sasmira/swade-fr-content/-/raw/master/module/module.json

### Modules requis pour le français
Pour traduire les éléments de base de FoundryVTT (interface), il vous faut installer et activer le module suivant :
> https://gitlab.com/baktov.sugar/foundryvtt-lang-fr-fr

La traduction du système fonctionne directement, cependant les compendiums nécessitent d'installer et activer le module Babele pour être traduit :
> https://gitlab.com/riccisi/foundryvtt-babele

### Modules recommandés
- Dice so Nice, pour avoir des dés 3D : https://gitlab.com/riccisi/foundryvtt-dice-so-nice
- Search Anywhere : https://gitlab.com/riccisi/foundryvtt-search-anywhere (pour ne pas perdre top de temps à chercher une technique)


## Nous rejoindre
1. Vous pouvez retrouver toutes les mises à jour en français pour FoundryVTT sur le discord officiel francophone
2. Lien vers [Discord Francophone](https://discord.gg/pPSDNJk)

## L'équipe SWADE actuelle des contributeur (par ordre alphabétique)
- BoboursToutCool
- Cyril "Gronyon" Ronseaux
- Kyane von Schnitzel
- LeRatierBretonnien
- Sasmira
- U~man 

## Remerciements, vielen danke & Many thanks to :
1. Sasmira et LeRatierBretonnien pour leur travail sur la [communauté Francophone de Foundry](https://discord.gg/pPSDNJk)
2. Kyane von Schnitzel et U~man pour leur travail sur la traduction de la feuille de personnage
3. Yannick "Torgan" Le Guédart et X.O. de Vorcen pour leurs aides et la traduction en français de SWADE.

## Contribuer
Vous êtes libre de contribuer et proposer après fork des corrections, modifications. Essayez de respecter 3 règles :
1. Assurez-vous de bien être à jour par rapport à la branche référente.
2. Des messages de commit clair et précis permettent une relecture rapide du code.
3. Limitez-vous si possible à une Feature par demande de Merge pour ne pas bloquer le processus.
