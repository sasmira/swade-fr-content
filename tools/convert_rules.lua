local jsonList = { 'old-rules.json' }

package.path = package.path .. ";luajson/?.lua"
local JSON = require'json'

for _, jsonFile in pairs(jsonList) do 
  local f= io.open(jsonFile, "r")
  local jsonIn = f:read("*a")
  f:close()

  local output = {}
  local jsonInData = JSON.decode(jsonIn)
  for index, entry in pairs(jsonInData.entries) do 
    output[entry.id] = { 
      name = entry.name, 
      text = entry.description
    }
  end

  local jsonOut = JSON.encode( output )
  f= io.open("new-rules.json", "w+")
  f:write(jsonOut)
  f:close()

end

