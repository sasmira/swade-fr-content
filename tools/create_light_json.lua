local jsonList = { 'swade-core-rules.swade-hindrances.json', 'swade-core-rules.swade-edges.json', 'swade-core-rules.swade-skills.json' }
local inFolder = '../module/compendiums/'
local outFolder = '../module/compendium_light/'

package.path = package.path .. ";luajson/?.lua"
local JSON = require'json'

for _, jsonFile in pairs(jsonList) do 
  local f= io.open(inFolder..jsonFile, "r")
  local jsonIn = f:read("*a")
  f:close()

  local jsonInData = JSON.decode(jsonIn)
  local jsonOutData = { label = jsonInData.label, entries = {}, mapping = { description = "data.description" } }
  for key, data in pairs(jsonInData.entries) do 
    jsonOutData.entries[key] = { id = data.id, name = data.name, description = data.description }
  end

  local jsonOut = JSON.encode(jsonOutData )
  f= io.open(outFolder..jsonFile, "w+")
  f:write(jsonOut)
  f:close()

end

